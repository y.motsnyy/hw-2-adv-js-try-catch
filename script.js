"use strict"; 

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const parentElem = document.createElement('div');
parentElem.id = 'root';
document.body.append(parentElem);
const booksList = document.createElement('ul');
parentElem.append(booksList);

function errorReport(book) {
  const keys = Object.keys(book);
    if (!keys.includes('author')) {
        return `The book ${book.name} has no author`;
    } else if (!keys.includes('name')) {
        return `The book (author: ${book.author} and price: ${book.price}) has no name`;
    } else if (!keys.includes('price')) {
        return `The book ${book.name} has no price`;
    }
}

function createBook(book) {
  const keys = Object.keys(book);
  if (keys.includes('author') && keys.includes('name') && keys.includes('price')) {
    const bookInfo = `<li><p>${book.author}, "${book.name}", ${book.price} $</p></li>`;
        booksList.insertAdjacentHTML('afterbegin', bookInfo);
    } else {
        throw new Error(`Error! ${errorReport(book)}`);
    }
}
    
function createBooksList(booksArray) {
    booksArray.forEach(book => {
        try {
          createBook(book);
        } catch (err) {
           console.error(err.message);
        }
    })
}

createBooksList(books);

