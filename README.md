Теоретический вопрос: Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

Ответ: При получении данных с сервера, они могут быть не полными или сдержать ошибки. Если json некорректен, JSON.parse генерирует ошибку, то есть скрипт «падает», что нас может не устраивать. Пользователь может ввести некорректные или неполные данные. Мы сами можем допускать ошибки.
